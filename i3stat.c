#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/sensors.h>
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <net/if_dl.h>
#include <net/if_media.h>
#include <net/if_types.h>
#include <netinet/in.h>
#include <net80211/ieee80211.h>
#include <net80211/ieee80211_ioctl.h>

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <machine/apmvar.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

static const char	*get_linkstate(int, int);
static int		 gettemp(const char *, double *);
static char		*if_getnwid(const char *);
static int		 if_getrssi(const char *, int *);
static int		 if_isup(const char *);
static char		*if_stat(const char *);
static int		 ifm_getmediatype(const char *);
static char		*ifm_getstatus(const char *);
static char		*in_getaddr(const char *);
static int		 link_isup(const char *);
static void		 link_status(const char *, int *, char *, size_t);
static char 		*loadavg(int);
static char 		*mkbats(void);
static char		*mkcpuspeeds(void);
static char		*mklefts(int);
static char		*mktemps(const char *);
static char		*mktimes(const char *);

static const struct ifmedia_status_description ifm_status_descriptions[] =
    IFM_STATUS_DESCRIPTIONS;
static const struct if_status_description if_status_descriptions[] =
    LINK_STATE_DESCRIPTIONS;
static int fd, s;

static const char *
get_linkstate(int mt, int link_state)
{
	const struct if_status_description *p;
	static char buf[8];

	for (p = if_status_descriptions; p->ifs_string != NULL; p++) {
		if (LINK_STATE_DESC_MATCH(p, mt, link_state))
			return (p->ifs_string);
	}
	snprintf(buf, sizeof(buf), "[#%d]", link_state);
	return buf;
}

static int
gettemp(const char *devname, double *temp)
{
	struct sensordev sensordev;
	struct sensor sensor;
	int dev, mib[5];
	size_t sdlen, slen;

	mib[0] = CTL_HW;
	mib[1] = HW_SENSORS;
	mib[2] = 0;
	mib[3] = 0;
	mib[4] = 0;
	sdlen = sizeof(sensordev);
	slen = sizeof(sensor);

	for (dev = 0; ; dev++) {
		mib[2] = dev;
		if (sysctl(mib, 3, &sensordev, &sdlen, NULL, 0) == -1) {
			if (errno == ENXIO)
				continue;
			if (errno == ENOENT)
				break;
			warn("sysctl");
			return -1;
		}
		if (!strncmp(sensordev.xname, devname, strlen(devname))) {
			mib[3] = SENSOR_TEMP;
			mib[4] = 0;
			if (sysctl(mib, 5, &sensor, &slen, NULL, 0) == -1) {
				if (errno != ENOENT) {
					warn("sysctl");
					return -1;
				}
			}
			*temp = (sensor.value - 273150000) / 1000000.0;
			return 0;
		}
	}
	return -1;
}

static char *
if_getnwid(const char *ifname)
{
	char buf[IEEE80211_NWID_LEN + 1];
	struct ifreq ifr;
	struct ieee80211_nwid nwid;
	int len;

	memset(&ifr, 0, sizeof(ifr));
	(void)strlcpy(ifr.ifr_name, ifname, sizeof(ifr.ifr_name));
	ifr.ifr_data = (caddr_t)&nwid;
	if (ioctl(s, SIOCG80211NWID, (caddr_t)&ifr) == -1) {
		warn("SIOCG80211NWID");
		return NULL;
	}
	if (nwid.i_len <= IEEE80211_NWID_LEN)
		len = nwid.i_len + 1;
	else
		len = IEEE80211_NWID_LEN + 1;
	strlcpy(buf, (char *)(nwid.i_nwid), len);
	return strdup(buf);
}

static int
if_getrssi(const char *ifname, int *rssi)
{
	struct ieee80211_bssid bssid;
	struct ieee80211_nodereq nr;

	memset(&bssid, 0, sizeof(bssid));
	(void)strlcpy(bssid.i_name, ifname, sizeof(bssid.i_name));
	if (ioctl(s, SIOCG80211BSSID, &bssid) == -1) {
		warn("SIOCG80211BSSID");
		return -1;
	}
	memset(&nr, 0, sizeof(nr));
	(void)strlcpy(nr.nr_ifname, ifname, sizeof(nr.nr_ifname));
	bcopy(bssid.i_bssid, &nr.nr_macaddr, sizeof(nr.nr_macaddr));
	if (ioctl(s, SIOCG80211NODE, &nr) == -1) {
		warn("SIOCG80211NODE");
		return -1;
	}
	*rssi = nr.nr_rssi;
	return 0;
}

static int
if_isup(const char *ifname)
{
	struct ifreq ifr;

	memset(&ifr, 0, sizeof(ifr));
	(void)strlcpy(ifr.ifr_name, ifname, sizeof(ifr.ifr_name));
	if (ioctl(s, SIOCGIFFLAGS, &ifr) == -1)
		warn("SIOCGIFFLAGS");
	if ((ifr.ifr_flags & 0xffff) & IFF_UP)
		return 1;
	return 0;
}

static char *
if_stat(const char *ifname)
{
	char buf[128], tmp[128];
	int rssi;
	char *addr, *ifmst, *nwid;

	addr = NULL;
	ifmst = NULL;
	nwid = NULL;

	snprintf(buf, sizeof(buf), "%s:", ifname);
	if (!if_isup(ifname)) {
		snprintf(tmp, sizeof(tmp), " down");
		strlcat(buf, tmp, sizeof(buf));
		return strdup(buf);
	}
	if (ifm_getmediatype(ifname) == IFM_IEEE80211) {
		if (if_getrssi(ifname, &rssi) == 0) {
			snprintf(tmp, sizeof(tmp), " %d dBm", rssi);
			strlcat(buf, tmp, sizeof(buf));
		}
		if ((nwid = if_getnwid(ifname)) != NULL && *nwid != '\0') {
			snprintf(tmp, sizeof(tmp), " %s", nwid);
			strlcat(buf, tmp, sizeof(buf));
		}
	}
	if ((addr = in_getaddr(ifname)) == NULL)
		snprintf(tmp, sizeof(tmp), " no IP");
	else
		snprintf(tmp, sizeof(tmp), " %s", addr);
	strlcat(buf, tmp, sizeof(buf));
	if (!link_isup(ifname)) {
		ifmst = ifm_getstatus(ifname);
		snprintf(tmp, sizeof(tmp), " (%s)", ifmst);
		strlcat(buf, tmp, sizeof(buf));
	}
	free(addr);
	free(ifmst);
	free(nwid);
	return strdup(buf);
}

static int
ifm_getmediatype(const char *ifname)
{
	struct ifmediareq ifmr;

	memset(&ifmr, 0, sizeof(ifmr));
	(void)strlcpy(ifmr.ifm_name, ifname, sizeof(ifmr.ifm_name));
	if (ioctl(s, SIOCGIFMEDIA, &ifmr) == -1) {
		warn("SIOCGIFMEDIA");
		return -1;
	}
	return IFM_TYPE(ifmr.ifm_current);
}

static char *
ifm_getstatus(const char *ifname)
{
	struct ifmediareq ifmr;
	const struct ifmedia_status_description *ifms;

	memset(&ifmr, 0, sizeof(ifmr));
	(void)strlcpy(ifmr.ifm_name, ifname, sizeof(ifmr.ifm_name));
	if (ioctl(s, SIOCGIFMEDIA, &ifmr) == -1) {
		warn("SIOCGIFMEDIA");
		return NULL;
	}
	for (ifms = ifm_status_descriptions; ifms->ifms_valid != 0; ifms++) {
		if (ifms->ifms_type != IFM_TYPE(ifmr.ifm_current))
			continue;
		return strdup(IFM_STATUS_DESC(ifms, ifmr.ifm_status));
	}
	return NULL;
}

static char *
in_getaddr(const char *ifname)
{
	struct ifreq ifr;
	struct sockaddr_in *sa;

	memset(&ifr, 0, sizeof(ifr));
	(void)strlcpy(ifr.ifr_name, ifname, sizeof(ifr.ifr_name));
	if (ioctl(s, SIOCGIFADDR, &ifr) == -1) {
		warn("SIOCGIFADDR");
		return NULL;
	}
	sa = (struct sockaddr_in *)&(ifr.ifr_addr);
	return strdup(inet_ntoa(sa->sin_addr));
}

static int
link_isup(const char *ifname)
{
	struct ifaddrs *ifap, *ifa;
	struct if_data *ifdata;
	int ls;

	if (getifaddrs(&ifap) == -1) {
		warn("getifaddrs");
		return 0;
	}
	for (ifa = ifap; ifa; ifa = ifa->ifa_next) {
		if (strcmp(ifname, ifa->ifa_name) != 0)
			continue;
		if (ifa->ifa_addr->sa_family == AF_LINK) {
			ifdata = ifa->ifa_data;
			ls = ifdata->ifi_link_state;
			break;
		}
	}
	freeifaddrs(ifap);
	return LINK_STATE_IS_UP(ls);
}

static void
link_status(const char *ifname, int *isup, char *state, size_t len)
{
	struct ifaddrs *ifap, *ifa;
	struct if_data *ifdata;
	struct sockaddr_dl *sdl;
	int ls;

	if (getifaddrs(&ifap) == -1) {
		warn("getifaddrs");
		return;
	}
	for (ifa = ifap; ifa; ifa = ifa->ifa_next) {
		if (strcmp(ifname, ifa->ifa_name) != 0)
			continue;
		if (ifa->ifa_addr->sa_family == AF_LINK) {
			ifdata = ifa->ifa_data;
			ls = ifdata->ifi_link_state;
			sdl = (struct sockaddr_dl *)ifa->ifa_addr;
			*isup = LINK_STATE_IS_UP(ls);
			strlcpy(state, get_linkstate(sdl->sdl_type, ls), len);
			break;
		}
	}
	freeifaddrs(ifap);
}

static char *
loadavg(int n)
{
	char buf[128], tmp[8];
	double loadavg[3];
	int i;

	memset(buf, 0, sizeof(buf));
	memset(tmp, 0, sizeof(tmp));
	if (getloadavg(loadavg, n) == -1) {
		warn("getloadavg");
		return NULL;
	}
	for (i = 0; i < n && i < 3; i++) {
		snprintf(tmp, sizeof(tmp), "%s%.2f", i > 0 ? " " : "",
		    loadavg[i]);
		strlcat(buf, tmp, sizeof(buf));
	}
	return strdup(buf);
}

static char *
mkbats(void)
{
	char buf[128], tmp[128];
	struct apm_power_info info;
	int batt;
	char *ac, *est;

	memset(buf, 0, sizeof(buf));
	memset(tmp, 0, sizeof(tmp));
	if (ioctl(fd, APM_IOC_GETPOWER, &info) == -1) {
		warn("ioctl");
		return NULL;
	}
	ac = info.ac_state == 1 ? "CHR" : "BAT";
	batt = info.battery_life;
	snprintf(buf, sizeof(buf), "%s %d%%", ac, batt);
	if (info.ac_state != 1 && (signed)info.minutes_left > 0) {
		est = mklefts(info.minutes_left);
		snprintf(tmp, sizeof(tmp), "  %s", est);
		strlcat(buf, tmp, sizeof(buf));
		free(est);
	}
	return strdup(buf);
}

static char *
mkcpuspeeds(void)
{
	char buf[128];
	int mib[2], cpuspeed;
	size_t len;

	memset(buf, 0, sizeof(buf));
	mib[0] = CTL_HW;
	mib[1] = HW_CPUSPEED;
	len = sizeof(cpuspeed);
	if (sysctl(mib, 2, &cpuspeed, &len, NULL, 0) == -1) {
		warn("sysctl");
		return NULL;
	}
	snprintf(buf, sizeof(buf), "%d MHz", cpuspeed);
	return strdup(buf);
}

static char *
mklefts(int left)
{
	char buf[128];
	int h, m;

	memset(buf, 0, sizeof(buf));
	for (h = 0, m = left; m > 60; h++, m -= 60);
	snprintf(buf, sizeof(buf), "%d:%.2d", h, m);
	return strdup(buf);
}

static char *
mktemps(const char *devname)
{
	char buf[128], tmp[128];
	double temp;

	memset(buf, 0, sizeof(buf));
	memset(tmp, 0, sizeof(tmp));
	if (gettemp(devname, &temp) == -1) {
		warn("gettemp");
		return NULL;
	}
	if (!strncmp(devname, "cpu", 3))
		snprintf(buf, sizeof(buf), "C: ");
	else if (!strncmp(devname, "acpitz", 6))
		snprintf(buf, sizeof(buf), "TZ: ");
	else
		snprintf(buf, sizeof(buf), "T: ");
	snprintf(tmp, sizeof(tmp), "%.2f °C", temp);
	strlcat(buf, tmp, sizeof(buf));
	return strdup(buf);
}

static char *
mktimes(const char *fmt)
{
	char buf[129];
	time_t t;
	struct tm *ttm;

	memset(buf, 0, sizeof(buf));
	t = time(NULL);
	if ((ttm = localtime(&t)) == NULL)
		err(1, "localtime");
	if (strftime(buf, sizeof(buf) - 1, fmt, ttm) == 0)
		err(1, "strftime");

	return strdup(buf);
}

int
main(int argc, char *argv[])
{
	char *acpitz0temp, *batt, *cpu0temp, *cpuspeed, *date, *em0stat,
	    *iwn0stat, *avgs;

	if ((s = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
		err(1, "socket");
	if ((fd = open("/dev/apm", O_RDONLY)) == -1)
		err(1, "opening /dev/apm failed");

	while (1) {
		iwn0stat = if_stat("iwn0");
		em0stat = if_stat("em0");
		batt = mkbats();
		avgs = loadavg(1);
		cpu0temp = mktemps("cpu0");
		acpitz0temp = mktemps("acpitz0");
		cpuspeed = mkcpuspeeds();
		date = mktimes("%a %Y-%m-%d %H:%M");
		printf("%s | %s | %s | %s | %s | %s | %s | %s\n", iwn0stat,
		    em0stat, batt, avgs, cpu0temp, acpitz0temp, cpuspeed,
		    date);
		fflush(stdout);
		free(iwn0stat);
		free(em0stat);
		free(batt);
		free(avgs);
		free(cpu0temp);
		free(acpitz0temp);
		free(cpuspeed);
		free(date);
		sleep(5);
	}

	return 0;
}
